<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
     
        <div class="clearfix"></div>
        <!--- Profile -->
        <div class="profile-info">
            <div class="col-xs-4">
              <a href="profile.html" class="rounded-image profile-image"><img src="{{ URL::to('images/users/image.jpeg')}}"></a>
            </div>
            <div class="col-xs-8">
                <div class="profile-text">Jumtukaayu Ligueey</div>
              
            </div>
        </div>
        <!--- Divider -->
        <div class="clearfix"></div>
        <hr class="divider" />
        <div class="clearfix"></div>
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
            <li class='has_sub'>
            <a href='{{ route('generation.home') }}' id="active-generation">
            <i class='icon-home-3'></i>
            <span>Dashboard</span> 
            </a>
            </li>
    
            <ul>
            <li>
            <a href='{{ route('home.dashboard') }}' id="active-home">
            <span>Dashboard v1</span>
            </a>
            </li>
            </ul>
            </li>


            
            
             <li>
            <a href='{{ route('home.types') }}' id="active-type">
            <i class='fa fa-group'></i>
            <span>Scolarité</span> 
            </a>
            </li>
            <li class='has_sub'>
            <a href='javascript:void(0);'>
            <i class='fa fa-child'></i>
            <span>Eleves</span> 
            <span class="pull-right">
            <i class="fa fa-angle-down"></i>
            </span>
            </a>
            <ul>
            <li>
            <a href='{{ route('show.eleve') }}' id="active-eleve-table">
            <span>Table</span>
            </a>
            </li>
            <li>
            <a href='{{ route('get_add_eleve') }}' id="active-eleve-add">
            <span>Ajouter</span>
            </a>
            </li>
            </ul>
            </li>
            <li class='has_sub'>
            <a href='javascript:void(0);'>
            <i class='fa fa-child'></i>
            <span>Professeurs</span> 
            <span class="pull-right">
            <i class="fa fa-angle-down"></i>
            </span>
            </a>
            <ul>
            <li>
            <a href='{{ route('show.entres') }}' id="active-entres-table">
            <span>Table</span>
            </a>
            </li>
            <li>
            <a href='{{ route('get_add_entres') }}' id="active-entres-add">
            <span>Ajouter</span>
            </a>
            </li>
            </ul>
            </li>
            <li class='has_sub'>
            <a href='javascript:void(0);'>
            <i class='fa fa-child'></i>
            <span>Personel</span> 
            <span class="pull-right">
            <i class="fa fa-angle-down"></i>
            </span>
            </a>
            <ul>
            <li>
            <a href='{{ route('show.personel') }}' id="active-personel-table">
            <span>Table</span>
            </a>
            </li>
            <li>
            <a href='{{ route('get_add_personel') }}' id="active-personel-add">
            <span>Ajouter</span>
            </a>
            </li>
            </ul>
            </li>
            <li class='has_sub'>
            <a href='javascript:void(0);'>
            <i class='fa fa-check-circle-o'></i>
            <span>Pointage</span> 
            <span class="pull-right">
            <i class="fa fa-angle-down"></i>
            </span>
            </a>
            <ul>
            <!-- <li>
            <a href='{{ route('show.pointage') }}' id="active-pointage-table">
            <span>Table</span>
            </a>
            </li> -->
            <li>
            <a href='{{ route('get_add_pointage') }}' id="active-pointage-add">
            <span>Ajouter</span>
            </a>
            </li>
            </ul>
            </li>
            <li>
            <a href='{{ route('generation.home') }}' id="active-generation">
            <i class='icon-home-3'></i>
            <span>Roles</span> 
            </a>
            </li>
            <li>
            <a href='{{ route('generation.home') }}' id="active-generation">
            <i class='icon-home-3'></i>
            <span>Droits</span> 
            </a>
            </li>
            <li>
            <a href='{{ route('generation.home') }}' id="active-generation">
            <i class='icon-home-3'></i>
            <span>Comptabilités</span> 
            </a>
            </li>
            <li>
            <a href='{{ route('generation.home') }}' id="active-generation">
            <i class='fa fa-line-chart'></i>
            <span>Statistiques</span> 
            </a>
            </li>
            <li>
            <a href='{{ route('generation.home') }}' id="active-generation">
            <i class='icon-home-3'></i>
            <span>Rapport de plateforme</span> 
            </a>
            </li>
            <li>
            <a href='{{ route('generation.home') }}' id="active-generation">
            <i class='icon-home-3'></i>
            <span>Authentifications</span> 
            </a>
            </li>

        <li class='has_sub'>
            <a href='javascript:void(0);'>
            <i class='fa fa-table'></i>
            <span>Entres</span> 
            <span class="pull-right">
            <i class="fa fa-angle-down"></i>
            </span>
            </a>
            <ul>
            <li>
            <a href='{{ route('show.entres') }}' id="active-entres-table">
            <span>Table</span>
            </a>
            </li>
            <li>
            <a href='{{ route('get_add_entres') }}' id="active-entres-add">
            <span>Ajouter</span>
            </a>
            </li>
            </ul>
            </li>
             <li class='has_sub'>
            <a href='javascript:void(0);'>
            <i class='fa fa-table'></i>
            <span>Sorties</span> 
            <span class="pull-right">
            <i class="fa fa-angle-down"></i>
            </span>
            </a>
            <ul>
            <li>
            <a href='{{ route('show.sorties') }}' id="active-sorties-table">
            <span>Table</span>
            </a>
            </li>
            <li>
            <a href='{{ route('get_add_sorties') }}' id="active-sorties-add">
            <span>Ajouter</span>
            </a>
            </li>
            </ul>
            </li>
            </ul>                  
              <div class="clearfix"></div>
        </div>
    <div class="clearfix"></div>
    
</div>
    
</div>
