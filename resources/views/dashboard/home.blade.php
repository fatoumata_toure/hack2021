@extends('main')

@section('title')
  Dashboard
@endsection
@section('styles')
<link rel="stylesheet" href="assets/css/main.css" charset="utf-8">
@endsection
@section('contents')




<div class="row">

    <div class="col-md-12">
      <div class="widget">
        <div class="widget-header transparent">
          <h2 class="text-center"><strong>PROFESSEURS</strong></h2>
          <div class="additional-btn">
            <a href="{{ route('show.entres') }}" class="infos-dashboard">Plus d'options ...</a>
          </div>
        </div>
        <div class="widget-content">
          <div class="table-responsive">
            <table data-sortable class="table table-hover table-striped">
              <thead>
                    <thead>
                          <tr>
                              <th>Classe</th>
                              <th>Date de naissance</th>
                              <th>N°Telephone</th>
                              <th>Nombe de classe</th>
                              <th>Nobre d'absence</th>
                              <th>Adrersse</th>
                          </tr>
                      </thead>
              </thead>

               <tbody>
                        @foreach($entres as $entre)
                          <tr>

                              <td>{{ $entre->type->name }}</td>
                              <td>{{ $entre->date }}</td>
                              <td>{{ $entre->nfacture }}</td>
                              <td>{{ $entre->quantite}}</td>
                              <td>{{ $entre->prix_uni}}</td>
                              <td>{{ $entre->fourni }}</td>

                          </tr>
                        @endforeach

                      </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>

    <div class="col-md-12">
      <div class="widget">
        <div class="widget-header transparent">
          <h2 class="text-center"><strong>ELEVES</strong></h2>
          <div class="additional-btn">
            <a href="{{ route('show.eleve') }}" class="infos-dashboard">Plus d'options ...</a>
          </div>
        </div>
        <div class="widget-content">
          <div class="table-responsive">
            <table data-sortable class="table table-hover table-striped">
              <thead>
                    <thead>
                          <tr>
                              <th>classe</th>
                              <th>Date de naissance</th>
                              <th>N°Telephone</th>
                              <th>Moyenne</th>
                              <th>Rang</th>
                              <th>Parents ou Tuteurs</th>
                          </tr>
                      </thead>
              </thead>

               <tbody>
                        @foreach($eleve as $eleve)
                          <tr>

                              <td>{{ $eleve->type->name }}</td>
                              <td>{{ $eleve->date }}</td>
                              <td>{{ $eleve->nfacture }}</td>
                              <td>{{ $eleve->quantite}}</td>
                              <td>{{ $eleve->prix_uni}}</td>
                              <td>{{ $eleve->fourni }}</td>

                          </tr>
                        @endforeach

                      </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>


    <div class="col-md-12">
      <div class="widget">
        <div class="widget-header transparent">
          <h2 class="text-center"><strong>PERSONNEL</strong></h2>
          <div class="additional-btn">
            <a href="{{ route('show.personel') }}" class="infos-dashboard">Plus d'options ...</a>
          </div>
        </div>
        <div class="widget-content">
          <div class="table-responsive">
            <table data-sortable class="table table-hover table-striped">
              <thead>
                    <thead>
                          <tr>
                              <th>classe</th>
                              <th>Date de naissance</th>
                              <th>N°Telephone</th>
                              <th>Code postal</th>
                              <th>N°d'identité</th>
                              <th>Adresse PERSONNEL</th>
                          </tr>
                      </thead>
              </thead>

               <tbody>
                        @foreach($personel as $personel)
                          <tr>

                              <td>{{ $personel->type->name }}</td>
                              <td>{{ $personel->date }}</td>
                              <td>{{ $personel->nfacture }}</td>
                              <td>{{ $personel->quantite}}</td>
                              <td>{{ $personel->prix_uni}}</td>
                              <td>{{ $personel->fourni }}</td>

                          </tr>
                        @endforeach

                      </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>


</div>
@endsection
@section('scripts')

<script>
       $('#active-home').addClass('active');
</script>
@endsection