@extends('main')
@section('title')
PERSONEL
@endsection


@section('contents')
    <div class="row">

      <div class="col-md-12">
        <div class="widget">
          <div class="widget-header">
            <h2 class="text-center"><strong>PERSONEL</strong></h2>

            <div class="additional-btn">
           <a href="{{ route('get_add_personel') }}"><button class="btn btn-success pull-right">Ajouter</button></a>
            </div>
          </div>
          <div class="widget-content">
          <br>
            <div class="table-responsive">
              <form class='form-horizontal' role='form'>
              <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>NOM</th>
                              <th>Date de naissance</th>
                              <th>N°Téléphone</th>
                              <th>RUE</th>
                              <th>Nombre d'heure</th>
                              <!-- <th>Parents ou Tuteurs</th>
                              <th>Scolarite</th> -->
                              <th>Adresse</th>
                          </tr>
                      </thead>


                      <tbody>
                        @foreach($personel as $personel)
                          <tr>

                              <td>{{ $personel->type->name }}</td>
                              <td>{{ date('d/m/Y',strtotime($personel->date)) }}</td>
                              <td>{{ $personel->nfacture }}</td>
                              <td>{{ $personel->quantite}}</td>
                              <td>{{ $personel->prix_uni}}</td>

                              <td><a href="{{ route('single.client',$personel->id)}}">{{ $personel->fourni }}</a></td>
                              <td>{!! $personel->solde = $personel->quantite * $personel->prix_uni !!}</td>
                              <td>
                          <div class="btn-group btn-group-xs">
                           <a href="{{ route('get_edit_personel',$personel->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('destroy.personel',$personel->id) }}" class="btn btn-defaultt"><i class="fa fa-trash"></i></a>

                          </div>
                        </td>

                          </tr>
                        @endforeach

                      </tbody>
                  </table>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
@section('scripts')
  <!-- Page Specific JS Libraries -->
  <script src="{{ URL::to('assets/libs/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ URL::to('assets/libs/jquery-datatables/js/dataTables.bootstrap.js') }}"></script>
  <script src="{{ URL::to('assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
  <script src="{{ URL::to('assets/js/pages/datatables.js') }}"></script>
  <script>
       $('#active-eleve-table').addClass('active');
</script>
@endsection
