@extends('main')
@section('title')
ELEVES
@endsection


@section('contents')
    <div class="row">

      <div class="col-md-12">
        <div class="widget">
          <div class="widget-header">
            <h2 class="text-center"><strong>ELEVES</strong></h2>

            <div class="additional-btn">
           <a href="{{ route('get_add_eleve') }}"><button class="btn btn-success pull-right">Ajouter</button></a>
            </div>
          </div>
          <div class="widget-content">
          <br>
            <div class="table-responsive">
              <form class='form-horizontal' role='form'>
              <table id="datatables-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>Classe</th>
                              <th>Date de naissance</th>
                              <th>N°Téléphone</th>
                              <th>Moyennes</th>
                              <th>Rang</th>
                              <th>Parents ou Tuteurs</th>
                              <th>Scolarite</th>
                              <th>Adresse</th>
                          </tr>
                      </thead>


                      <tbody>
                        @foreach($eleve as $eleve)
                          <tr>

                              <td>{{ $eleve->type->name }}</td>
                              <td>{{ date('d/m/Y',strtotime($eleve->date)) }}</td>
                              <td>{{ $eleve->nfacture }}</td>
                              <td>{{ $eleve->quantite}}</td>
                              <td>{{ $eleve->prix_uni}}</td>

                              <td><a href="{{ route('single.client',$eleve->id)}}">{{ $eleve->fourni }}</a></td>
                              <td>{!! $eleve->solde = $eleve->quantite * $eleve->prix_uni !!}</td>
                              <td>
                          <div class="btn-group btn-group-xs">
                           <a href="{{ route('get_edit_eleve',$eleve->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('destroy.eleve',$eleve->id) }}" class="btn btn-defaultt"><i class="fa fa-trash"></i></a>

                          </div>
                        </td>

                          </tr>
                        @endforeach

                      </tbody>
                  </table>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
@section('scripts')
  <!-- Page Specific JS Libraries -->
  <script src="{{ URL::to('assets/libs/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ URL::to('assets/libs/jquery-datatables/js/dataTables.bootstrap.js') }}"></script>
  <script src="{{ URL::to('assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
  <script src="{{ URL::to('assets/js/pages/datatables.js') }}"></script>
  <script>
       $('#active-eleve-table').addClass('active');
</script>
@endsection
