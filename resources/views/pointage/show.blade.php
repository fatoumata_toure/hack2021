@extends('main')
@section('title')
POINTER
@endsection


@section('contents')
    <div class="row">

      <div class="col-md-12">
        <div class="widget">
          <div class="widget-header">
            <h2 class="text-center"><strong>PERSONEL</strong></h2>
            <div class="row pointme-row">
          <div class="col-md-4 col-md-offset-4 pointme-col">
          <div class="illustration"><span class="point-label">POINT</span><span class="me-label">me</span>
          </div>
          <form id="pointme-form">
          <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">
          <i class="fa fa-phone"></i></span>
          <input type="tel" name="phone" class="form-control" placeholder="Entrez votre numéro de téléphone" aria-describedby="basic-addon1">
          </div>
          <div class="submit-pointme-btn-container">
          <button class="btn btn-success bg-vgreen pointme_submit_btn">Pointer</button>
          </div>
          </form>
          </div>
            </div>
                              <td><a href="{{ route('single.client',$pointage->id)}}">{{ $pointage->fourni }}</a></td>
                              <td>{!! $pointage->solde = $pointage->quantite * $pointage->prix_uni !!}</td>
                              <td>
                          <div class="btn-group btn-group-xs">
                           <a href="{{ route('get_edit_pointage',$pointage->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('destroy.pointage',$pointage->id) }}" class="btn btn-defaultt"><i class="fa fa-trash"></i></a>

                          </div>
                        </td>

                          </tr>
                        @endforeach

                      </tbody>
                  </table>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
@section('scripts')
  <!-- Page Specific JS Libraries -->
  <script src="{{ URL::to('assets/libs/jquery-datatables/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ URL::to('assets/libs/jquery-datatables/js/dataTables.bootstrap.js') }}"></script>
  <script src="{{ URL::to('assets/libs/jquery-datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
  <script src="{{ URL::to('assets/js/pages/datatables.js') }}"></script>
  <script>
       $('#active-eleve-table').addClass('active');
</script>
@endsection
